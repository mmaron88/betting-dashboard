import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BetsComponent } from './components/bets/bets.component';
import { BetComponent } from './components/bet/bet.component';
import { ButtonComponent } from './components/button/button.component';
import { HeadingComponent } from './components/heading/heading.component';
import { TicketComponent } from './components/ticket/ticket.component';
import { LayoutComponent } from './components/layout/layout.component';
import { InputComponent } from './components/input/input.component';
import { StoreModule } from '@ngrx/store';
import { betsReducer } from './reducers/bets.reducer';

@NgModule({
  declarations: [
    AppComponent,
    BetsComponent,
    BetComponent,
    ButtonComponent,
    HeadingComponent,
    TicketComponent,
    LayoutComponent,
    InputComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({ selectedBets: betsReducer }, {}),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
