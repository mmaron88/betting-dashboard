import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AppState, Bet, Choice, SelectedBet } from '../../model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.scss'],
})
export class BetComponent {
  @Input() bet: Bet;
  @Output() onSelectBet = new EventEmitter<string>();
  private selectedBets: SelectedBet[];

  constructor(private store: Store<AppState>) {
    this.store.select('selectedBets').subscribe((selectedBets) => {
      this.selectedBets = selectedBets;
    });
  }

  selectBet(choice: string) {
    this.onSelectBet.emit(choice);
  }

  isSelected(choice: Choice) {
    const matchedBet = this.selectedBets.find(({ id }) => id === this.bet.id);

    if (!matchedBet) {
      return false;
    }

    return matchedBet.choice === choice;
  }
}
