import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit, OnChanges {
  // TODO type variant with Enum
  // Could not be done for now - lack of knowledge
  @Input() variant = 'normal';
  @Input() selected: boolean = false;
  isNormal: boolean;
  isSmall: boolean;
  isSelected: boolean;

  ngOnInit() {
    this.isNormal = this.variant === 'normal';
    this.isSmall = this.variant === 'small';
    this.isSelected = this.selected;
  }

  ngOnChanges() {
    // For now only selected is dynamic value
    this.isSelected = this.selected;
  }
}
