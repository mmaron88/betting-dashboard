import { Component, OnInit } from '@angular/core';
import { BetService } from '../../services/bet.service';
import { AppState, Bet, Choice } from '../../model';
import { Store } from '@ngrx/store';
import { SelectBetAction } from '../../actions/bets.actions';

@Component({
  selector: 'app-bets',
  templateUrl: './bets.component.html',
})
export class BetsComponent implements OnInit {
  bets: Bet[];

  constructor(private betService: BetService, private store: Store<AppState>) {}

  ngOnInit() {
    this.getBets();
  }

  private getBets() {
    this.betService.getBets().subscribe((value) => {
      this.bets = value;
    });
  }

  selectBet(bet: Bet, choice: Choice) {
    this.store.dispatch(new SelectBetAction({ ...bet, choice }));
  }
}
