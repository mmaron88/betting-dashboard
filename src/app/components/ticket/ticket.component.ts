import { Component, OnInit } from '@angular/core';
import { AppState, SelectedBet } from '../../model';
import { Store } from '@ngrx/store';
import { RemoveBetAction } from '../../actions/bets.actions';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
})
export class TicketComponent implements OnInit {
  selectedBets: SelectedBet[] = [];
  stakeValue: number;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store
      .select<SelectedBet[]>('selectedBets')
      .subscribe((selectedBets) => {
        this.selectedBets = selectedBets;
      });
  }

  get totalOddsCount() {
    return this.selectedBets.reduce((count, { odds, choice }) => {
      count += odds[choice];
      return count;
    }, 0);
  }

  get totalPossiblePay() {
    return this.totalOddsCount * this.stakeValue;
  }

  removeBet(bet: SelectedBet) {
    this.store.dispatch(new RemoveBetAction(bet.id));
  }

  setStakeValue(stakeValue: number) {
    this.stakeValue = stakeValue;
  }
}
