import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() id: string = 'input';
  @Input() name: string;
  @Input() min: number = 10;
  @Input() type: string = 'text';
  @Output() onValueChange = new EventEmitter<string>();

  inputValue: string = '10';

  ngOnInit() {
    this.onValueChange.emit(this.inputValue);
  }

  constructor() {}

  onChange(value: string) {
    this.onValueChange.emit(value);
  }
}
