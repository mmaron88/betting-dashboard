import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bet, RawBet } from '../model';

@Injectable({
  providedIn: 'root',
})
export class BetService {
  private BETS_URL = `${environment.API_URL}/bets`;

  constructor(private http: HttpClient) {}

  public getBets(): Observable<Bet[]> {
    // Map raw bets from API to app model of bet
    return this.http.get<RawBet[]>(this.BETS_URL).pipe(
      map<RawBet[], Bet[]>((bets) =>
        bets.map<Bet>(({ id, teams, draw }) => ({
          id,
          eventName: `${teams[0].name} - ${teams[1].name}`,
          odds: { 0: teams[0].win, 1: draw, 2: teams[1].win },
        }))
      )
    );
  }
}
