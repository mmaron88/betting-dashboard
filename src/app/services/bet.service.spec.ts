import { TestBed } from '@angular/core/testing';

import { BetService } from './bet.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('BetService', () => {
  const betsMock = [
    {
      id: 1,
      teams: [
        {
          name: 'Real Madrid',
          win: 1.2,
        },
        {
          name: 'Real Sociedad',
          win: 4.5,
        },
      ],
      draw: 12.3458,
    },
  ];

  beforeEach(() => {
    const serviceStub = {
      getBets: () => of(betsMock),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: BetService, useValue: serviceStub }],
    });
  });

  it('should get proper bets', () => {
    const service: BetService = TestBed.get(BetService);
    service.getBets().subscribe((bets) => {
      expect(bets).toEqual(betsMock);
    });
  });
});
