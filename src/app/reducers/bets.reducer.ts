import { SelectedBet } from '../model';
import { BetActions, REMOVE_BET, SELECT_BET } from '../actions/bets.actions';

export function betsReducer(state: SelectedBet[] = [], action: BetActions) {
  switch (action.type) {
    // TODO do better toggle for the item with replace at index etc
    case SELECT_BET:
      // if the same id was clicked as the selected one we filter out bet
      if (state.some(({ id }) => id === action.payload.id)) {
        const data = state.filter(({ id }) => id !== action.payload.id);

        // Moreover if for the same id, choice has been changed, add it again to the selected bets
        if (
          state.some(
            ({ id, choice }) =>
              id === action.payload.id && choice !== action.payload.choice
          )
        ) {
          return data.concat(action.payload);
        }

        return data;
      }

      return state.concat(action.payload);

    case REMOVE_BET:
      return state.filter((bet) => action.payload !== bet.id);

    default:
      return state;
  }
}
