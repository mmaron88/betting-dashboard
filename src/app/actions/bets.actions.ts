import { Action } from '@ngrx/store';
import { SelectedBet } from '../model';

export const SELECT_BET = '[Bets] Select Bet';
export const REMOVE_BET = '[Bets] Remove Bet';

export class SelectBetAction implements Action {
  readonly type = SELECT_BET;

  constructor(public payload: SelectedBet) {}
}

export class RemoveBetAction implements Action {
  readonly type = REMOVE_BET;

  constructor(public payload: number) {}
}

export type BetActions = SelectBetAction | RemoveBetAction;
