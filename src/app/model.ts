// Store
export interface AppState {
  selectedBets: SelectedBet[];
}

// Bets
export type OddsDictionary = Record<0 | 1 | 2, number>;

export enum Choice {
  Home = 0,
  Draw = 1,
  Away = 2,
}

interface RawTeam {
  name: string;
  win: number;
}

export interface RawBet {
  id: number;
  teams: [RawTeam, RawTeam];
  draw: number;
}

export interface Bet {
  id: number;
  eventName: string;
  odds: OddsDictionary;
}

export interface SelectedBet extends Bet {
  choice: Choice;
}
